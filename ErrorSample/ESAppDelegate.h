//
//  ESAppDelegate.h
//  ErrorSample
//
//  Created by Hiroyuki Shigeta on 2014/01/29.
//  Copyright (c) 2014年 Hiroyuki Shigeta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
