//
//  ESViewController.h
//  ErrorSample
//
//  Created by Hiroyuki Shigeta on 2014/01/29.
//  Copyright (c) 2014年 Hiroyuki Shigeta. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    IOError, UnknownError
} ErrorCode;

@interface ESViewController : UIViewController
- (void) doSomeMethod:(ErrorCode *)error;
- (void) doSomeMethodWithError:(ErrorCode **)error;
@end
