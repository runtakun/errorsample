//
//  ESViewController.m
//  ErrorSample
//
//  Created by Hiroyuki Shigeta on 2014/01/29.
//  Copyright (c) 2014年 Hiroyuki Shigeta. All rights reserved.
//

#import "ESViewController.h"

@interface ESViewController ()

@end

@implementation ESViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ErrorCode *error = nil;
    [self doSomeMethod:error];
    NSLog(@"処理結果1：%@", error);//(null)と出力
    
    ErrorCode *error2 = nil;
    [self doSomeMethodWithError:&error2];
    NSLog(@"処理結果2：%d", error2);// 1 と出力
    if (error2 == IOError) {
        NSLog(@"I/Oエラー！");
    } else if (error2 == UnknownError) {
        NSLog(@"あんのうんエラー！");
    }
}

- (void) doSomeMethod:(ErrorCode *)error {
    error = UnknownError;
}

- (void) doSomeMethodWithError:(ErrorCode **)error {
    *error = UnknownError;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
