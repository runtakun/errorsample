//
//  main.m
//  ErrorSample
//
//  Created by Hiroyuki Shigeta on 2014/01/29.
//  Copyright (c) 2014年 Hiroyuki Shigeta. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESAppDelegate class]));
    }
}
